/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "gtest/gtest.h"

using ::testing::Test;

TEST(UnsupportedInPureCapTest, DoesSkip) {
  GTEST_UNSUPPORTED_IN_PURE_CAP();
  EXPECT_EQ(0, 1);
}

class Fixture : public Test {
 protected:
  void SetUp() override {
    GTEST_UNSUPPORTED_IN_PURE_CAP() << "skipping all tests for this fixture";
  }
};

TEST_F(Fixture, SkipsOneTest) {
  EXPECT_EQ(5, 7);
}

TEST_F(Fixture, SkipsAnotherTest) {
  EXPECT_EQ(99, 100);
}
